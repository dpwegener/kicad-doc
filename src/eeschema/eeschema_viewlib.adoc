:experimental:

[[viewlib]]
== Symbol Library Browser

=== Introduction

The Symbol Library Browser allows you to quickly examine the content of symbol
libraries. The Symbol Library Viewer can be accessed by clicking
image:images/icons/library_browser_24.png[Library viewer icon] icon on the main
toolbar, **View** -> **Symbol Library Browser...**, or clicking **Select With
Browser** in the "Choose Symbol" window.

image::images/eeschema_viewlib_choose.png[alt="eeschema_viewlib_choose_png",scaledwidth="60%"]

[[viewlib---main-screen]]
=== Viewlib - main screen

image::images/eeschema_viewlib_select_library.png[alt="eeschema_viewlib_select_library_png",scaledwidth="95%"]

To examine the contents of a library, select a library from the list on the left hand pane.
All symbols in the selected library will appear in the second pane.  Select a symbol name
to view the symbol.

image::images/eeschema_viewlib_select_component.png[alt="eeschema_viewlib_select_component_png",scaledwidth="95%"]

[[viewlib-top-toolbar]]
=== Symbol Library Browser Top Toolbar

The top tool bar in Symbol Library Brower is shown below.

image::images/toolbar_viewlib.png[alt="images/toolbar_viewlib.png",scaledwidth="95%"]

The available commands are:

[width="100%",cols="20%,80%",]
|=======================================================================
|image:images/icons/library_browser_24.png[Symbol selection icon]
|Selection of the symbol which can be also selected in the displayed
list.

|image:images/icons/lib_previous_24.png[Previous symbol icon]
|Display previous symbol.

|image:images/icons/lib_next_24.png[Next symbol icon]
|Display next symbol.

|image:images/icons/refresh_24.png[] image:images/icons/zoom_in_24.png[]
image:images/icons/zoom_out_24.png[] image:images/icons/zoom_fit_in_page_24.png[]
|Zoom tools.

|image:images/icons/morgan1_24.png[] image:images/icons/morgan2_24.png[]
|Selection of the representation (normal or alternate) if an alternate
representation exists.

|image:images/toolbar_viewlib_part.png[alt="images/toolbar_viewlib_part.png",width="70%"]
|Selection of the unit for symbols that contain multiple units.

|image:images/icons/datasheet_24.png[icons/datasheet_png]
|If they exist, display the associated documents.

|image:images/icons/add_symbol_to_schematic_24.png[Add symbol to schematic icon]
|Close the browser and place the selected symbol in the schematic.
|=======================================================================
